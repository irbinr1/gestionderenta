package Core;

import Model.Customer;
import Model.Movie;
import Model.RentalItem;

import java.util.ArrayList;
import java.util.List;

public class MovieRentalSystem {
    private String name;
    private List<RentalItem> rentalItems;
    private List<Customer> customers;

    public MovieRentalSystem(String name) {
        this.name = name;
        this.rentalItems = new ArrayList<>();
        this.customers = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RentalItem> getRentalItems() {
        return rentalItems;
    }

    public void setRentalItems(List<RentalItem> rentalItems) {
        this.rentalItems = rentalItems;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public void addItem(RentalItem rentalItem) {
        this.rentalItems.add(rentalItem);
    }

    public void registerCustomer(Customer customer) {
        this.customers.add(customer);
    }

    public void rentMovieTocustomer( String cliente, String movie) {
        // Verificamos si existe el cliente
        Customer user = null;
        for (Customer item : this.customers){
            if (item.getCustomerName().equals(cliente)) {
                user = item;
                break;
            }
        }

        if ( user == null ) {
            System.out.println("Cliente no encontrado");
            return;
        }

        // Verificamos si existe la película
        RentalItem item = null;
        for ( RentalItem valor : this.rentalItems) {
            if (valor.getItemName().equals(movie)) {
                item = valor;
            }
        }

        if ( item == null ) {
            System.out.println("Película no encontrada");
            return;
        }

        if ( item.getAvailable()) {
            user.agregarPelicula(item);
            item.setAvailable(false);
            System.out.println("Película rentada exitosamente.");
        }
        else {
            System.out.println("Pelicula no disponible");
        }


    }

    public void returnMovie(String cliente, String movie) {
        Customer usuario = null;
        for (Customer client : this.customers) {
            if ( client.getCustomerName().equals(cliente)) {
                usuario = client;
                break;
            }
        }

        if ( usuario == null ) {
            System.out.println("Cliente no registrado en el sistema");
            return;
        }

        RentalItem rentalItem = null;
        int indice = 0;
        int tamanio = this.rentalItems.size();
        for (int i = 0; i < tamanio; i++) {
            RentalItem pelicula = this.rentalItems.get(i);
            if (pelicula.getItemName().equals(movie)){
                rentalItem = pelicula;
                indice = i;
                break;
            }
        }

        if ( rentalItem == null ) {
            System.out.println("No existe esa película en el sistema");
            System.out.println("Actualmente tiene los siguientes películas rentadas: ");
            int contador = 0;
            for (RentalItem pelicula : usuario.getRentalItems()) {
                System.out.println(++contador + ". " + pelicula.getItemName());
            }
            return;
        }
        usuario.getRentalItems().get(indice).setAvailable(true);
        usuario.getRentalItems().remove(indice);
        System.out.println("Película devuelta exitosamente");


    }

    public void showAllItems() {
        for ( RentalItem item : this.rentalItems ) {
            if ( item.getAvailable() ) {
                System.out.println(item.getItemName());
            }
        }
    }

    public void showAllCustomers() {
        for ( Customer customer : this.customers ) {
            customer.showCustomerDetails();
        }
    }

    public void buscarCliente(String cliente) {
        for (Customer client : this.customers) {
            if (client.getCustomerName().equals(cliente)) {
                client.showCustomerDetails();
            }
        }
    }

}
