package App;

import Core.MovieRentalSystem;
import Model.Customer;
import Model.Movie;
import Model.RentalItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        MovieRentalSystem movieRentalSystems = new MovieRentalSystem("Cineplaznet");
        List<RentalItem> rentalItems = new ArrayList<RentalItem>(
                List.of(
                        new Movie("Spider Man", 1, true, "Sam Raimi", (short)126,"+14","Acción, Aventura"),
                        new Movie("Civil War", 2, true, "Jorge Franks", (short)129, "+14", "Acción, Aventura"),
                        new Movie("Batman", 3, true, "Cristopher Nolan", (short)126, "+15", "Acción, Suspenso, Aventura"),
                        new Movie("Hopperheimer", 4, true, "Tom Valdi", (short)135, "+18", "Historico, Suspenso, Drama")
                ));

        List<Customer> customers = new ArrayList<Customer>(
                List.of(
                        new Customer("Andres", 1),
                        new Customer("Fatima", 2),
                        new Customer("Alejandra", 3),
                        new Customer("Pedro", 4)
                ));
        movieRentalSystems.setRentalItems(rentalItems);
        movieRentalSystems.setCustomers(customers);

        Scanner input = new Scanner(System.in);
        salir:
        while (true) {
            System.out.print("-----------------------------------------\nQue operación desea realizar\n-----------------------------------------\n1. Rentar películas" +
                    "\n2. Devoluciones de películas\n3. Mostrar detalles de películas\n4. Mostrar detalles de clientes" +
                    "\n5. Buscar cliente\n6. Salir\nIngrese la opción a realizar: ");
            int opcion = input.nextInt();
            input.nextLine();
            switch (opcion) {
                case 1:
                    System.out.print("Ingrese nombre del cliente: ");
                    String cliente1 = input.nextLine();
                    System.out.print("Ingrese nombre de la película: ");
                    String pelicula1 = input.nextLine();
                    movieRentalSystems.rentMovieTocustomer(cliente1, pelicula1);
                    break;
                case 2:
                    System.out.print("Ingrese nombre del cliente: ");
                    String cliente2 = input.nextLine();
                    System.out.print("Ingrese nombre de la película: ");
                    String pelicula2 = input.nextLine();
                    movieRentalSystems.returnMovie(cliente2, pelicula2);
                    break;
                case 3:
                    for (RentalItem item : movieRentalSystems.getRentalItems()) {
                        item.showDetails();
                    }
                    break;
                case 4:
                    movieRentalSystems.showAllCustomers();
                    break;
                case 5:
                    System.out.print("Ingrese nombre del cliente: ");
                    String usuario = input.nextLine();
                    movieRentalSystems.buscarCliente(usuario);
                    break;
                case 6:
                    break salir;
                default:
                    System.out.println("La opción ingresada es incorrecta");

            }
        }



    }

}