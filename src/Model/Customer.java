package Model;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String customerName;
    private int customerID;
    private List<RentalItem> rentalItems;

    public Customer(String customerName, int customerID) {
        this.customerName = customerName;
        this.customerID = customerID;
        this.rentalItems = new ArrayList<>();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public List<RentalItem> getRentalItems() {
        return rentalItems;
    }

    public void setRentalItems(List<RentalItem> rentalItems) {
        this.rentalItems = rentalItems;
    }

    public void agregarPelicula(RentalItem rentalItem) {
        this.rentalItems.add(rentalItem);
    }
    public void showCustomerDetails() {
        System.out.print("Cliente: " + this.customerName + "\nID: " + this.customerID + "\nPelículas rentadas: ");
        if (this.rentalItems.size() > 0) {
            int longitud = this.rentalItems.size();
            for (int i = 0; i < longitud; i++) {
                if (i != longitud - 1) {
                    System.out.print(this.rentalItems.get(i).itemName + ", ");
                }
                else {
                    System.out.print(this.rentalItems.get(i).itemName + ".");
                    System.out.println();
                }
            }
        }
        else {
            System.out.println(" --- ");
        }
        System.out.println("------------------------------------------------------");
    }
}
