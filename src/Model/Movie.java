package Model;

public class Movie extends RentalItem{
    private String director;
    private short duration;
    private String ageRating;
    private String genre;

    public Movie(String itemName, int itemID, boolean available, String director, short duration, String ageRating, String genre) {
        super(itemName, itemID, available);
        this.director = director;
        this.duration = duration;
        this.ageRating = ageRating;
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public short getDuration() {
        return duration;
    }

    public void setDuration(short duration) {
        this.duration = duration;
    }

    public String getAgeRating() {
        return ageRating;
    }

    public void setAgeRating(String ageRating) {
        this.ageRating = ageRating;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public void showDetails() {
        super.showDetails();
        System.out.println("Director: " + this.director +
                "\nDuración: " + this.duration + "\nClasificación: " + this.ageRating +
                "\nGénero: " + this.genre);
        System.out.println("---------------------------------------------------");
    }
}
